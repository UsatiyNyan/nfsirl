A project made by BMSTU students

UsatiyNyanWS - workspace for Kirill Illarionov. Responsible for robot scripts, some CV scripts, TCP and UDP connection and custom serial connection.

Anton-Rampage-WS - workspace for Anton Ilin. Responsible for WiFi triangulation/mapping and CV

sanya - workspace for Alexander Dolgavin. Responsible for QT application.

Robot designed by Kirill Illarionov

the entire assembly is made by Kirill Illarionov and Anton Ilin

you can see it in action:
https://youtu.be/W2XQr-0o7Bk
